/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.logginSpringBoot.controller;

import com.example.logginSpringBoot.domain.Person;
import com.example.logginSpringBoot.service.personService;
import com.example.logginSpringBoot.service.sesionService;
import java.util.HashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * http://localhost:8080/login
 * @author Ricard
 */
@Controller
public class controller {
    HashMap<String, String> identidad = new HashMap<>();
    HashMap<String, Integer> intentos = new HashMap<>();
    
    @Autowired
    private sesionService sesionService;
    @Autowired
    private personService personService;
    /**formulario
     * 
     * @param model
     * @return 
     */
    @GetMapping("/login")
    public String formulario(Model model) {
        String clientMessage = "";
        String nombre = "";
        model.addAttribute("clientMessage", clientMessage);
        model.addAttribute("nombre", nombre);
        return "portal";
    }
    /**metodo login
     * 
     * @param nombre
     * @param passw
     * @param model
     * @return 
     */
    @PostMapping("/login")
    public String login(
            @RequestParam(name = "name") String nombre,
            @RequestParam(name = "passw") String passw,
            Model model
    ) {
        this.identidad = sesionService.loginService(nombre, passw);
        model.addAttribute("nombre", nombre);
        if (!sesionService.bloqueoService(intentos, nombre)) {
            if (!identidad.get("codigo").equals("error")) {
                this.intentos = sesionService.intentosService(intentos, nombre, false);
                //model.addAttribute("clientMessage", "Bienvenido " + nombre + " !");
                return "welcome";
            } else {
                this.intentos = sesionService.intentosService(intentos, nombre, true);
                model.addAttribute("clientMessage", "" + nombre + ", te quedan " + intentos.get(nombre) + "/3 intentos.");
                return "portal";
            }
        } else {
            //model.addAttribute("clientMessage", "Ha excedido el numero de intentos con el usuario " + nombre + " !");
            return "blocked";
        }
    }
    
    @GetMapping("/registro_nuevo")
    public String nuevoRegistro(Model model){
        return "formulario";
    }
    
    @PostMapping("/person")
    public String submitForm(Person person, Model model) {
        if (personService.PreSavePerson(person)) {
            String nombre = person.getName();
            model.addAttribute("clientMessage", "El usuario se ha registrado correctamente!");
            model.addAttribute("nombre", nombre);
        }
        model.addAttribute("clientMessage", "El usuario no se ha registrado correctamente, puede intentar el Log-in con un usuario valido o registrar un usuario nuevo.");
        model.addAttribute("nombre", "");
        return "portal";
    }
}

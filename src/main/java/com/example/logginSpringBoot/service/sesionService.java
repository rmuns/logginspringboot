/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.example.logginSpringBoot.service;

import com.example.logginSpringBoot.domain.Login;
import java.util.HashMap;

/**
 *
 * @author Ricard
 */
public interface sesionService {
    public HashMap<String, String> loginService(String nombre, String passw);
    public Login getPersonById(Long id);
    public int getPersonById(String name, String passw);
    public boolean bloqueoService (HashMap<String, Integer> intentos, String nombre);
    public HashMap<String, Integer> intentosService(HashMap<String, Integer> intentos, String nombre, boolean restaIntentos);
}

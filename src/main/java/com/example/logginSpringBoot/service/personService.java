/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.example.logginSpringBoot.service;

import com.example.logginSpringBoot.domain.Person;
import java.util.List;

/**
 *
 * @author Ricard
 */
public interface personService {
    public boolean PreSavePerson(Person person);
    public Person savePerson(Person person);
    public List<Person> getAllPersons();
}

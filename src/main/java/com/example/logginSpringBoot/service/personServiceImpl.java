/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.logginSpringBoot.service;
import com.example.logginSpringBoot.dao.personDAO;
import com.example.logginSpringBoot.domain.Person;
import static java.util.Arrays.equals;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Ricard
 */
@Service
public class personServiceImpl implements personService{
    @Autowired
    private personDAO personDAO;
    
    private boolean checkNewPerson (Person person) {
        boolean disponibilidad = false;
        if (!person.getName().isBlank()) {
            String nombre = person.getName();
            List<Person> lista = getAllPersons();
            
            for (Person persona : lista) {
                if (!nombre.equals(persona.getName())) {
                    disponibilidad = true;
                }
            }
        }
        return disponibilidad;
    }
    
    @Override
    public boolean PreSavePerson(Person person){
        boolean insertCorrecto = false;
        if(checkNewPerson(person)){
            savePerson(person);
            insertCorrecto = true;
        }
        return insertCorrecto;
    }
    
    @Override
    public Person savePerson(Person person){
        return personDAO.save(person);
    }
    
    @Override
    public List<Person> getAllPersons(){
        return personDAO.findAll();
    }
}

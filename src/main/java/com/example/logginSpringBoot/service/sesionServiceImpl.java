/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.logginSpringBoot.service;

import com.example.logginSpringBoot.dao.logginDAO;
import com.example.logginSpringBoot.domain.Login;
import java.util.HashMap;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Ricard
 */
@Service
public class sesionServiceImpl implements sesionService{
    
    @Autowired
    private logginDAO logginDAO;
    
    /**loginService
     * 
     * @param nombre
     * @param passw
     * @return 
     */
    @Override
    public HashMap<String, String> loginService(String nombre, String passw) {
        HashMap<String, String> identidad = new HashMap<>();
        
        String sesion = checkIdentidad(nombre, passw);
        identidad.put("name",nombre);
        identidad.put("passw",passw);
        identidad.put("codigo",sesion);
        
        return identidad;
    }
    /**checkIdentidad
     * Simula la consulta a la base de datos
     * @param nombre
     * @param passw
     * @return Respuesta con alguna identificacion de sesion o un mensaje de error
     */
    private String checkIdentidad(String nombre, String passw){
        boolean login = false;
        String codSesion = "error";
        //comproba l'usuari i contrasenya
        Login persona;
        int id = getPersonById(nombre,passw);
        if (id!=0){
            login = true;
        }
        //envia codi de loggin
        if(login){
            codSesion = "correcte";
        }
        return codSesion;
    }
    /**
     * 
     * @param id
     * @return 
     */
    @Override
    public Login getPersonById(Long id){
        return logginDAO.findById(id).orElse(null);
    }
    /**
     * 
     * @param name
     * @param passw
     * @return 
     */
    @Override
    public int getPersonById(String name, String passw){
        List<Login> lista = logginDAO.findAll();
        int id = 0;
        for(Login in : lista){
            if(in.getName().equals(name)){
                if (in.getPassw().equals(passw)) {
                    id = (int) in.getId();
                }
            }
        }
        return id;
    }
    /**bloqueoService
     * 
     * @param intentos
     * @param nombre
     * @return Retorna un bolean true si el usuario esta bloqueado
     */
    @Override
    public boolean bloqueoService (HashMap<String, Integer> intentos, String nombre) {
        boolean usuariosBloqueado = false;
        //Si el usuario no a comenzado los intentos el bloqueo es false
        if (intentos.containsKey(nombre)) {
            if (intentos.get(nombre) == 0) {
                usuariosBloqueado = true;
            }
        }
        return usuariosBloqueado;
    }
    /**intentosService
     * 
     * @param intentos
     * @param nombre
     * @param restaIntentos
     * @return Sobreescribe el hashmap de controller para controlar los intentos
     */
    @Override
    public HashMap<String, Integer> intentosService(HashMap<String, Integer> intentos, String nombre, boolean restaIntentos){
        int numeroMaxIntentosNuevo = 2;
        int numeroMaxIntentosOld = 3;
        if(!intentos.containsKey(nombre)){
            intentos.put(nombre, numeroMaxIntentosNuevo);
        } else {
            if(restaIntentos == false){
                intentos.put(nombre, numeroMaxIntentosOld);
            } else {
                intentos.put(nombre, intentos.get(nombre) - 1);
            }
        }
        return intentos;
    }
}

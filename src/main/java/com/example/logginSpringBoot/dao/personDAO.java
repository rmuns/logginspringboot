/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.example.logginSpringBoot.dao;

import com.example.logginSpringBoot.domain.Person;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Ricard
 */
public interface personDAO extends JpaRepository<Person, Long>{
    
}
